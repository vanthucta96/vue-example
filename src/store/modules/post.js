import axios from "axios"

const state = {
    posts: [],
    post:{}
}

const getters = {
    totalPost: state => state.posts.length
}

const actions = {
    allPost({commit}) {
        axios.get('http://localhost:3000/posts').then((res) => {
        commit('setposts', res.data)
      })
    },
    deletePost(context,post_id) {
        console.log(post_id)
        axios.delete('http://localhost:3000/posts/'+post_id).then(() => {
            this.dispatch('post/allPost')
      })
      .catch()
    },
    getOnePost({commit},id){
      axios.get('http://localhost:3000/posts/'+id).then((res) => {
            commit('setOnePost', res.data)
      })
      .catch()
    },
    editUser(context, post) {
      axios.put('http://localhost:3000/posts/'+post.id, {
          title: post.title,
          author: post.author,
          content: post.content,
      })
          .then(() => this.dispatch('post/allPost'));
  },

  }

  const mutations = {
    setposts(state, data) {
      state.posts = data
    },
    setOnePost(state,data){
      state.post = data
    }
  }
  
  export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
  }