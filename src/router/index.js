import Vue from 'vue'
import VueRouter from 'vue-router'
import PostList from '../components/PostList.vue'
import UpdatePost from '../components/UpdatePost.vue'
Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  routes: [

    {
      path: '/',
      component: PostList,
      name: 'post.list',
    },
    {
        path: '/post/:id/edit',
        name: 'post.edit',
        component: UpdatePost
    }
      
  ]
})
